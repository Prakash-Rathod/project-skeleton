# FOOD ORDERING POC USING AXON #

The food ordering makes heavy use of the Axon framework to manage its internal data flow. like food order, add or remove
items, confirm order etc. It uses a layered architecture with the layers cleanly separated via message buses.

### Software Versions

```
    1. Red Hat OpenJDK 11
    2. MySQL 5.7
    3. Maven 3.6.3
    4. IntelliJ IDEA 2021.2 (Edu) with google-java-format plugin configued
    5. Axon Server 4.5.7
```

### Technologies

* [Axon Framework](https://docs.axoniq.io/reference-guide/architecture-overview)
* OpenJDK 11 + [Kotlin 1.5.31](https://kotlinlang.org/)

## Folder Structure

```
axon-food-ordering
├── src
│ ├── main
│ │ ├── java
│ │ │ └── com.fo
│ │ │   ├── FoodOrderingDemoApplication.java
│ │ │   ├── command
│ │ │   │ └── FoodCart.java
│ │ │   ├── coreapi
│ │ │   │ ├── commnads.kt
│ │ │   │ ├── events.kt
│ │ │   │ ├── exceptions.kt
│ │ │   │ └── queries.kt
│ │ │   ├── gui
│ │ │   │ └── FoodOrderingController.java
│ │ │   └── query
│ │ │     ├── FoodCartProjector.java
│ │ │     └── FoodCartView.kt
│ │ └── resources
│ │   └── application.properties
│ └── test
│   └── java
│     └── com.fo
│       └── FoodOrderingDemoApplicationTests.java
├── docs
│ └── Axon.postman_collection.json
├── README.md
├── HELP.md
├── pom.xml
├── mvnw
└── mvnw.cmd
```