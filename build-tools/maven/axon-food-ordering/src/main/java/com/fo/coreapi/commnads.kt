/* Copyright © 2021 Deltafixes Tehcnologies. All rights reserved. */
package com.fo.coreapi

import org.axonframework.commandhandling.RoutingKey
import org.axonframework.modelling.command.TargetAggregateIdentifier

class CreateFoodCartCommand(
    @RoutingKey val foodCartId: Int
)


data class SelectProductCommand(
    @TargetAggregateIdentifier val foodCartId: Int,
    val productId: String,
    val quantity: Int
)

data class DeselectProductCommand(
    @TargetAggregateIdentifier val foodCartId: Int,
    val productId: String,
    val quantity: Int
)

data class ConfirmOrderCommand(
    @TargetAggregateIdentifier val foodCartId: Int,
)