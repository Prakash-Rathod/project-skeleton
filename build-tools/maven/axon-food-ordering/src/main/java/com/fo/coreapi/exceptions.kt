/* Copyright © 2021 Deltafixes Tehcnologies. All rights reserved. */
package com.fo.coreapi

class ProductDeselectionException(message: String) : Exception(message)