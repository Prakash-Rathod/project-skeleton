/* Copyright © 2021 Deltafixes Tehcnologies. All rights reserved. */
package com.fo.coreapi

data class FindFoodCartQuery(val foodCartId: Int)

class RetrieveProductOptionQuery