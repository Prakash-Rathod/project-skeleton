/* Copyright © 2021 Deltafixes Tehcnologies. All rights reserved. */

package com.fo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodOrderingDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodOrderingDemoApplication.class, args);
	}

}
