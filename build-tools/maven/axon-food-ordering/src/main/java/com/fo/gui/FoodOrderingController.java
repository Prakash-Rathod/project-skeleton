/* Copyright © 2021 Deltafixes Tehcnologies. All rights reserved. */
package com.fo.gui;

import com.fo.coreapi.CreateFoodCartCommand;
import com.fo.coreapi.DeselectProductCommand;
import com.fo.coreapi.FindFoodCartQuery;
import com.fo.coreapi.SelectProductCommand;
import com.fo.query.FoodCartView;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.CompletableFuture;

@RestController
public class FoodOrderingController {

  private final CommandGateway commandGateway;
  private final QueryGateway queryGateway;

  public FoodOrderingController(CommandGateway commandGateway, QueryGateway queryGateway) {
    this.commandGateway = commandGateway;
    this.queryGateway = queryGateway;
  }

  @PostMapping("/create/{foodCartId}")
  public void handle(@PathVariable("foodCartId") Integer foodCartId) {
    commandGateway.send(new CreateFoodCartCommand(foodCartId));
  }

  @PostMapping("/select")
  public void selectProduct(
      @RequestParam("foodCartId") Integer foodCartId,
      @RequestParam("productId") String productId,
      @RequestParam("quantity") Integer quantity) {
    commandGateway.send(new SelectProductCommand(foodCartId, productId, quantity));
  }

  @PostMapping("/deselect")
  public void deselectProduct(
      @RequestParam("foodCartId") Integer foodCartId,
      @RequestParam("productId") String productId,
      @RequestParam("quantity") Integer quantity) {
    commandGateway.send(new DeselectProductCommand(foodCartId, productId, quantity));
  }

  @GetMapping("/foodcart/{foodCartId}")
  public CompletableFuture<FoodCartView> get(@PathVariable("foodCartId") Integer foodCartId) {
    return queryGateway.query(
        new FindFoodCartQuery(foodCartId), ResponseTypes.instanceOf(FoodCartView.class));
  }
}
