/* Copyright © 2021 Deltafixes Tehcnologies. All rights reserved. */
package com.fo.coreapi

data class FoodCartCreatedEvent(
        val foodCartId: Int
)

data class ProductSelectedEvent(
        val foodCartId: Int,
        val productId: String,
        val quantity: Int
)

data class ProductDeselectedEvent(
        val foodCartId: Int,
        val productId: String,
        val quantity: Int
)

data class OrderConfirmedEvent(
        val foodCartId: Int,
)