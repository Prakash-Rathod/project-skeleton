package com.fo.query

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import javax.persistence.ElementCollection
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.Id

@Entity
data class FoodCartView(
    @Id val foodCartId: Int,
    @ElementCollection(fetch = FetchType.EAGER) val products: MutableMap<String, Int>
){
    fun addProducts(productId: String, amount: Int) =
            products.compute(productId) { _, quantity -> (quantity ?: 0) + amount }

    fun removeProducts(productId: String, amount: Int) {
        val leftOverQuantity = products.compute(productId) { _, quantity -> (quantity ?: 0) - amount }
        if (leftOverQuantity == 0) {
            products.remove(productId)
        }
    }
}

@Repository
interface FoodCartViewRepository: JpaRepository<FoodCartView, Int>