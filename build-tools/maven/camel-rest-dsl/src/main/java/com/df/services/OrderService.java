package com.df.services;

import com.df.payloads.Order;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService {
    private List<Order> orders = new ArrayList<>();

    @PostConstruct
    public void init() {
        orders.add(new Order(1001, "Mobile", 7500));
        orders.add(new Order(1002, "Laptop", 35000));
        orders.add(new Order(1003, "Tv", 25000));
    }

    public Order addOrder(Order order) {
        orders.add(order);
        return order;
    }

    public List<Order> getOrders() {
        return orders;
    }
}
