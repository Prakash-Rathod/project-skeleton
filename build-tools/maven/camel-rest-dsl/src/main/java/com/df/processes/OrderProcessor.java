package com.df.processes;

import com.df.payloads.Order;
import com.df.services.OrderService;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.processing.Completion;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import java.util.Set;

@Component
public class OrderProcessor implements Processor {
    @Autowired
    private OrderService orderService;

    @Override
    public void process(Exchange exchange) throws Exception {
        orderService.addOrder(exchange.getIn().getBody(Order.class));
    }
}
