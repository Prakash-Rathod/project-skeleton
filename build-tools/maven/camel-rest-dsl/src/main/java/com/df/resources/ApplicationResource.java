package com.df.resources;

import com.df.payloads.Order;
import com.df.processes.OrderProcessor;
import com.df.services.OrderService;
import org.apache.camel.BeanInject;
import org.apache.camel.Expression;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class ApplicationResource extends RouteBuilder {
    @Value("${server.port}")
    private String port;

    @Autowired
    private OrderService orderService;
    @BeanInject
    private OrderProcessor orderProcessor;

    @Override
    public void configure() throws Exception {
        restConfiguration().component("servlet").port(port).bindingMode(RestBindingMode.json);
        // Get rest points
        rest().get("/hello").produces(MediaType.APPLICATION_JSON_VALUE).route().setBody(constant("Welcome ....")).endRest();
        rest().get("/getOrders").produces(MediaType.APPLICATION_JSON_VALUE).route().setBody(constant(orderService.getOrders())).endRest();

        // Post rest points
        rest().post("/addOrder").consumes(MediaType.APPLICATION_JSON_VALUE).type(Order.class).outType(Order.class).route().process(orderProcessor).endRest();
    }
}
