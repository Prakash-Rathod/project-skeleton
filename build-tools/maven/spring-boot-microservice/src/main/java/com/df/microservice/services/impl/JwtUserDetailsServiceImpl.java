/* Copyright © 2021 Deltafixes Tehcnologies. All rights reserved. */
package com.df.microservice.services.impl;

import com.df.microservice.services.CustomerService;
import com.df.microservice.services.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * {@link com.df.microservice.services.JwtUserDetailsService} implementation, core service logic should be defined over here.
 * This must implement base security {@link UserDetailsService}
 */
@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService, JwtUserDetailsService {
    private final CustomerService customerService;

    @Autowired
    public JwtUserDetailsServiceImpl(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        String password = customerService.getPassword(username);
        return new User(username, password, new ArrayList<>());
    }
}
