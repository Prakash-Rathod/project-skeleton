/* Copyright © 2021 Deltafixes Tehcnologies. All rights reserved. */
package com.df.microservice.payloads

import com.df.microservice.entities.Customer
import io.swagger.annotations.ApiModelProperty

/**
 * POJO for JWT Request
 */
data class JwtRequest(
    @ApiModelProperty(notes = "User Name", example = "peter@gmail.com")
    val userName: String,
    @ApiModelProperty(notes = "Password", example = "peter@123")
    val password: String
)

/**
 * POJO for JWT Response
 */
data class JwtResponse(
    @ApiModelProperty(notes = "Token", example = "slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6")
    val token: String,
    val customer: Customer
)