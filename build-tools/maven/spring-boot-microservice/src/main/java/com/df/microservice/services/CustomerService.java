/* Copyright © 2021 Deltafixes Tehcnologies. All rights reserved. */
package com.df.microservice.services;

import com.df.microservice.entities.Address;
import com.df.microservice.entities.Customer;
import com.df.microservice.payloads.CustomerCSVBean;

import java.util.List;

/**
 * Customer service and their implementation are
 *
 * @see com.df.microservice.services.impl.CustomerServiceImpl
 */

public interface CustomerService {
    Customer save(Customer customer);

    Customer update(Long cId, Customer customer);

    Customer addAddresses(Long cId, List<Address> address);

    Customer delete(Long cId);

    Customer findByEmail(String email);

    Customer findBycId(Long cId);

    List<Customer> getAll();

    List<Customer> findByName(String name);

    List<CustomerCSVBean> getExportCustomerData();

    String getPassword(String email);
}
