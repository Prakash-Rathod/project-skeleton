/* Copyright © 2021 Deltafixes Tehcnologies. All rights reserved. */
package com.df.microservice.payloads

import com.opencsv.bean.CsvBindByName
import com.opencsv.bean.CsvBindByPosition
import java.math.BigInteger

/**
 * POJO for holding Customer related data, also used in export & import customer data as CSV file.
 */
data class CustomerCSVBean(
    @CsvBindByName(column = "Customer ID", required = true)
    @CsvBindByPosition(position = 0, required = true)
    var customerId: BigInteger? = null,

    @CsvBindByName(column = "Email", required = true)
    @CsvBindByPosition(position = 1, required = true)
    val email: String? = null,

    @CsvBindByName(column = "Name", required = true)
    @CsvBindByPosition(position = 2, required = true)
    val name: String? = null,

    @CsvBindByName(column = "phoneNumber", required = true)
    @CsvBindByPosition(position = 3, required = true)
    val phoneNumber: String? = null
)
