/* Copyright © 2021 Deltafixes Tehcnologies. All rights reserved. */
package com.df.microservice.controllers;

import com.df.microservice.annotations.ApiResponseDoc;
import com.df.microservice.component.Messages;
import com.df.microservice.constants.ApiConstant;
import com.df.microservice.constants.SwaggerConstant;
import com.df.microservice.entities.Address;
import com.df.microservice.entities.Customer;
import com.df.microservice.payloads.DataResponseBody;
import com.df.microservice.payloads.SuccessResponseBody;
import com.df.microservice.services.CustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.List;

import static org.springframework.http.HttpStatus.OK;

/**
 * This TEST controller are mainly expose customer REST end points.
 * Autowired and calling {@link CustomerService} method.
 */

@Api(tags = {SwaggerConstant.ApiTag.CUSTOMER}, value = "Customer API's")
@RestController
@Validated
@RequestMapping(value = ApiConstant.API_CUSTOMER)
public class CustomerController {

    private final CustomerService customerService;
    private final Messages message;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public CustomerController(CustomerService customerService, Messages message, PasswordEncoder passwordEncoder) {
        this.customerService = customerService;
        this.message = message;
        this.passwordEncoder = passwordEncoder;
    }

    @ApiResponseDoc
    @ApiOperation(value = "Creating a new customer", notes = "${CREATE_CUSTOMER}", hidden = true, response = SuccessResponseBody.class)
    @RequestMapping(value = "save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> save(@Valid @RequestBody Customer customer, HttpServletRequest request) {
        // Encode customer password
        customer.setPassword(passwordEncoder.encode(customer.getPassword()));
        Customer cObj = customerService.save(customer);
        return new ResponseEntity<>(new SuccessResponseBody(OK.value(), OK.name(), message.get("data.added.success"), new DataResponseBody(cObj)), OK);
    }

    @ApiResponseDoc
    @ApiOperation(value = "Update existing customer", notes = "${UPDATE_CUSTOMER}", hidden = true, response = SuccessResponseBody.class)
    @RequestMapping(value = "/update/{cId}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> update(@PathVariable("cId") @Min(1) Long cId, @Valid @RequestBody Customer customer) {
        Customer updatedCustomer = customerService.update(cId, customer);
        return new ResponseEntity<>(new SuccessResponseBody(OK.value(), OK.name(), message.get("data.updated.success"), new DataResponseBody(updatedCustomer)), OK);
    }

    @ApiResponseDoc
    @ApiOperation(value = "Add or Update or Delete customer addresses", hidden = true, notes = "${ADD_UPDATE_DELETE_CUSTOMER_ADDRESS}", response = SuccessResponseBody.class)
    @RequestMapping(value = "/addresses/{cId}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addAddresses(@PathVariable("cId") @Min(1) Long cId, @RequestBody List<@Valid Address> addresses) {
        Customer customer = customerService.addAddresses(cId, addresses);
        return new ResponseEntity<>(new SuccessResponseBody(OK.value(), OK.name(), message.get("data.added.success"), new DataResponseBody(customer)), OK);
    }

    @ApiResponseDoc
    @ApiOperation(value = "Delete customer", notes = "${DELETE_CUSTOMER}", hidden = true, response = SuccessResponseBody.class)
    @RequestMapping(value = "/delete/{cId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteCustomer(@PathVariable("cId") @Min(1) Long cId) {
        Customer customer = customerService.delete(cId);
        return new ResponseEntity<>(new SuccessResponseBody(OK.value(), OK.name(), message.get("data.deleted.success"), new DataResponseBody(customer)), OK);
    }

    @ApiResponseDoc
    @ApiOperation(value = "Customer find by name", notes = "${GET_CUSTOMER_BY_NAME}", hidden = true, response = SuccessResponseBody.class)
    @RequestMapping(value = "/getByName", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findByCustomerName(@RequestParam @NotBlank String name, HttpServletRequest request) {
        List<Customer> customers = customerService.findByName(name);
        return new ResponseEntity<>(new SuccessResponseBody(OK.value(), OK.name(), message.get("data.found"), new DataResponseBody(customers)), OK);
    }

    @ApiResponseDoc
    @ApiOperation(value = "Get customer by cId", notes = "${GET_CUSTOMER_BY_ID}", hidden = true, response = SuccessResponseBody.class)
    @RequestMapping(value = "/{cId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getCustomer(@PathVariable("cId") @Min(1) Long cId) {
        Customer customer = customerService.findBycId(cId);
        return new ResponseEntity<>(new SuccessResponseBody(OK.value(), OK.name(), message.get("data.found"), new DataResponseBody(customer)), OK);
    }

    @ApiResponseDoc
    @ApiOperation(value = "Get all customers", notes = "${GET_CUSTOMERS}", hidden = true, response = SuccessResponseBody.class)
    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAll() {
        return new ResponseEntity<>(new SuccessResponseBody(OK.value(), OK.name(), message.get("data.found"), new DataResponseBody(customerService.getAll())), OK);
    }

}
