/* Copyright © 2021 Deltafixes Tehcnologies. All rights reserved. */
package com.df.microservice.services.impl;

import com.df.microservice.component.Messages;
import com.df.microservice.entities.Address;
import com.df.microservice.entities.Customer;
import com.df.microservice.exceptions.RecordNotFoundException;
import com.df.microservice.payloads.CustomerCSVBean;
import com.df.microservice.repositories.CustomerRepository;
import com.df.microservice.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * {@link CustomerService} implementation, core service logic should be defined over here only.
 * From the base repository {@link CustomerRepository} implemented methods are call in custom service implementation.
 */
@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final Messages message;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository, Messages message) {
        this.customerRepository = customerRepository;
        this.message = message;
    }

    @Override
    public Customer save(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public Customer update(Long cId, Customer customer) {
        // Checking whether customer is found or not.
        findBycId(cId);
        customer.setCId(cId);
        return save(customer);
    }

    @Override
    public Customer addAddresses(Long cId, List<Address> addresses) {
        Customer customer = findBycId(cId);
        customer.getAddresses().clear();
        customer.getAddresses().addAll(addresses);
        customer = save(customer);
        return customer;
    }

    @Override
    public Customer delete(Long cId) {
        // Checking whether customer is found or not.
        Customer customer = findBycId(cId);
        customerRepository.deleteById(cId);
        return customer;
    }

    @Override
    public Customer findByEmail(String email) {
        return customerRepository.findByEmail(email).orElseThrow(() -> new RecordNotFoundException(message.get("customer.not.found.with.email", email)));
    }

    @Override
    public Customer findBycId(Long cId) {
        return customerRepository.findById(cId).orElseThrow(() -> new RecordNotFoundException(message.get("customer.not.found", cId)));
    }

    @Override
    public List<Customer> getAll() {
        return customerRepository.findAll();
    }

    @Override
    public List<Customer> findByName(String name) {
        return customerRepository.findByName(name);
    }

    @Override
    public List<CustomerCSVBean> getExportCustomerData() {
        return customerRepository.getCustomers();
    }

    @Override
    public String getPassword(String email) {
        return customerRepository.getPassword(email).orElseThrow(() -> new RecordNotFoundException(message.get("customer.not.found.with.email", email)));
    }
}
