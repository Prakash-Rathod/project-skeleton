package com.df.microservice.constants;

public final class ApiConstant {
    public static final String API_VERSION_V1 = "v1";
    public static final String API_CUSTOMER = API_VERSION_V1 + "/customer";
    public static final String API_EXPORT_IMPORT = API_VERSION_V1 + "/export-import";
    public static final String API_AUTHENTICATION = API_VERSION_V1 + "/auth";

    private ApiConstant() {
        // hide
    }
}
