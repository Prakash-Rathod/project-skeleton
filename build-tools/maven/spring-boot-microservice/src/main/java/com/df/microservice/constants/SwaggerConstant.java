package com.df.microservice.constants;

public final class SwaggerConstant {
    public SwaggerConstant() {
        super();
    }

    public static final class ApiTag {
        public static final String CUSTOMER = "Customer";
        public static final String EXPORT_IMPORT = "Export Import";
        public static final String AUTHENTICATION = "Authentication";

        private ApiTag() {
            // to hide
        }
    }

    public static final class ResponseMessage {

        // HttpStatus 200
        public static final String OK_SUCCESSFUL = "The request completed successfully.";

        private ResponseMessage() {
            // to hide
        }
    }
}
