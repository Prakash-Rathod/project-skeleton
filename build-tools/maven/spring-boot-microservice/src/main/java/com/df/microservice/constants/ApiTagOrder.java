package com.df.microservice.constants;

public enum ApiTagOrder {
    CUSTOMER(SwaggerConstant.ApiTag.CUSTOMER, "", false),
    AUTHENTICATION(SwaggerConstant.ApiTag.AUTHENTICATION, "", false),
    EXPORT_IMPORT(SwaggerConstant.ApiTag.EXPORT_IMPORT, "", false);

    private final String name;

    private final String description;

    private final boolean isHidden;

    ApiTagOrder(final String name, final String description, final boolean isHidden) {
        this.name = name;
        this.description = description;
        this.isHidden = isHidden;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean isHidden() {
        return isHidden;
    }

}
