/* Copyright © 2021 Deltafixes Tehcnologies. All rights reserved. */
package com.df.microservice.configurations;

import kotlin.text.Charsets;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**
 * Application level configurations.
 */
@Configuration
public class ApplicationConfiguration {
    /*
     * Multilingual support configuration.
     */
    @Bean
    static MessageSource messageSource() {
        var messageResource = new ReloadableResourceBundleMessageSource();
        messageResource.setBasename("classpath:i18n/messages");
        messageResource.setDefaultEncoding(Charsets.UTF_8.name());
        return messageResource;
    }

    /*
     * Bean validation configuration, error become come from language property file.
     */
    @Bean
    public LocalValidatorFactoryBean getValidator() {
        var bean = new LocalValidatorFactoryBean();
        bean.setValidationMessageSource(messageSource());
        return bean;
    }
}
