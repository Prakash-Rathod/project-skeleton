/* Copyright © 2021 Deltafixes Tehcnologies. All rights reserved. */
package com.df.microservice.repositories

import com.df.microservice.entities.Customer
import com.df.microservice.payloads.CustomerCSVBean
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*

/**
 * It contains the full API of CrudRepository and PagingAndSortingRepository.
 * So it contains API for basic {@link Customer} CRUD operations and also API for pagination and sorting.
 * In short this will perform CRUD as well as Batch operation.
 *
 * Hibernate Fundamental
 * @see <a href="https://www.netsurfingzone.com/hibernate/spring-data-crudrepository-saveall-and-findall/">Hibernate Fundamental</a>
 *
 * Hibernate ResultTransformer
 * @see <a href="https://thorben-janssen.com/hibernate-resulttransformer/">Hibernate ResultTransformer</a>
 * @see <a href="https://newbedev.com/jpa-how-to-convert-a-native-query-result-set-to-pojo-class-collection">Hibernate ResultTransformer</a>
 *
 * ORM queries load from XML and mapping specific model
 * @see <a href="https://github.com/sohangp/spring-data-jpa-projections-example/blob/4272d5b665768ba08efaea0349b089cdaf189cba/src/main/resources/META-INF/orm.xml">ORM queries load from XML</a>
 *
 */
@Repository
interface CustomerRepository : JpaRepository<Customer, Long> {

    fun findByEmail(email: String): Optional<Customer>

    @Query(name = "customer.findByName")
    fun findByName(@Param("name") name: String): List<Customer>

    @Query(name = "customer.getCustomers", nativeQuery = true)
    fun getCustomers(): List<CustomerCSVBean>

    @Query(name = "customer.getPassword", nativeQuery = true)
    fun getPassword(@Param("email") email: String): Optional<String>

}