/* Copyright © 2021 Deltafixes Tehcnologies. All rights reserved. */
package com.df.microservice.configurations;

import com.df.microservice.constants.ApiTagOrder;
import com.df.microservice.constants.SwaggerConstant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.OperationBuilderPlugin;
import springfox.documentation.spi.service.contexts.OperationContext;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.common.SwaggerPluginSupport;
import springfox.documentation.swagger.web.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.ServletContext;
import java.util.*;

import static com.df.microservice.configurations.jwt.JwtUtil.AUTHORIZATION;
import static springfox.documentation.spi.DocumentationType.SWAGGER_2;

/**
 * Global API document configuration.
 */
@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
@PropertySource("classpath:api-swagger-doc.properties")
public class SwaggerDocumentation {

    public static final String VERSION_GROUP_NAME = "v1";
    public static final Contact DEFAULT_CONTACT = new Contact("Spring + MySQL + POC", "https://www.deltafixes.com/", "dummy@gmail.com");
    public static final ApiInfo DEFAULT_API_INFO =
            new ApiInfo("POC API", "POC RestFul API", VERSION_GROUP_NAME, "", DEFAULT_CONTACT, "", "", new ArrayList<>());
    private static final Set<String> DEFAULT_PRODUCES_AND_CONSUMES = new HashSet<>(Arrays.asList(MediaType.APPLICATION_JSON_VALUE));

    @Autowired
    private SwaggerCustomConfig propertyConfig;

    @Bean
    public SwaggerCustomConfig swaggerCustomConfig() {
        return new SwaggerCustomConfig();
    }

    /*
     * Swagger UI configuration
     */
    @Bean
    public UiConfiguration uiConfig() {
        return UiConfigurationBuilder
                .builder()
                .deepLinking(false)
                .displayOperationId(false)
                .defaultModelsExpandDepth(0)
                .defaultModelRendering(ModelRendering.MODEL)
                .displayRequestDuration(false)
                .docExpansion(DocExpansion.LIST)
                .filter(true)
                .maxDisplayedTags(null)
                .operationsSorter(OperationsSorter.ALPHA)
                .showExtensions(true)
                .tagsSorter(TagsSorter.ALPHA)
                .validatorUrl("")
                .build();
    }

    /*
     * User authentication API's configurations.
     */
    @Bean
    public Docket authAPI(ServletContext servletContext) {
        return new Docket(SWAGGER_2).apiInfo(DEFAULT_API_INFO).groupName("Authentication")
                .host(propertyConfig.getBaseUrl().replace("https://", "").replace("http://", ""))
                .useDefaultResponseMessages(false)
                .produces(DEFAULT_PRODUCES_AND_CONSUMES)
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .select()
                .paths(PathSelectors.regex(".*/auth.*")).build()
                .tags(new Tag(SwaggerConstant.ApiTag.EXPORT_IMPORT, "", 3), getApiOrderedTags());
    }

    /*
     * Customer API's configurations.
     */
    @Bean
    public Docket customerAPI(ServletContext servletContext) {
        return new Docket(SWAGGER_2).apiInfo(DEFAULT_API_INFO).groupName("Customer")
                .host(propertyConfig.getBaseUrl().replace("https://", "").replace("http://", ""))
                .protocols(Set.of(propertyConfig.getDefautProtocol()))
                .useDefaultResponseMessages(false)
                .produces(DEFAULT_PRODUCES_AND_CONSUMES)
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .select()
                .paths(PathSelectors.regex(".*/customer.*")).build()
                .tags(new Tag(SwaggerConstant.ApiTag.EXPORT_IMPORT, "", 2), getApiOrderedTags());
    }

    @Bean
    public Docket exportImportAPI(ServletContext servletContext) {
        return new Docket(SWAGGER_2).apiInfo(DEFAULT_API_INFO).groupName("Export and Import")
                .host(propertyConfig.getBaseUrl().replace("https://", "").replace("http://", ""))
                .useDefaultResponseMessages(false)
                .produces(DEFAULT_PRODUCES_AND_CONSUMES)
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .select()
                .paths(PathSelectors.regex(".*/export-import.*"))
                .build().tags(new Tag(SwaggerConstant.ApiTag.EXPORT_IMPORT, "", 3), getApiOrderedTags());
    }

    private ApiKey apiKey() {
        return new ApiKey(AUTHORIZATION, AUTHORIZATION, "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder().securityReferences(defaultAuth())
                .build();
    }

    private List<SecurityReference> defaultAuth() {
        final AuthorizationScope[] authorizationScopes = {new AuthorizationScope("global", "accessEverything")};
        return Collections.singletonList(new SecurityReference(AUTHORIZATION, authorizationScopes));
    }

    private Tag[] getApiOrderedTags() {
        final List<Tag> tags = new ArrayList<>();
        for (final ApiTagOrder tag : ApiTagOrder.values()) {
            if (propertyConfig.isHideInternalAPI() && tag.isHidden()) {
                continue;
            }
            tags.add(new Tag(tag.getName(), tag.getDescription(), tag.ordinal() + 2));
        }
        return tags.toArray(new Tag[0]);
    }

//    @Primary
//    @Component
//    public class CustomApiListingReferenceScanner extends ApiListingReferenceScanner {
//        @Override
//        @SuppressWarnings("deprecation")
//        public ApiListingReferenceScanResult scan(final DocumentationContext context) {
//            final MultiMap resourceGroupRequestMappings = new MultiValueMap();
//            final ApiSelector selector = context.getApiSelector();
//            final Iterable<RequestHandler> matchingHandlers = from(context.getRequestHandlers()).filter(selector.getRequestHandlerSelector());
//            for (final RequestHandler handler : matchingHandlers) {
//                final Optional<Api> apiAnnotation = handler.findControllerAnnotation(Api.class);
//                if (apiAnnotation.isPresent() && (propertyConfig.isHideInternalAPI() && apiAnnotation.get().hidden())) {
//                    continue;
//                }
//                final ResourceGroup resourceGroup = new ResourceGroup(handler.groupName(), handler.declaringClass(), 0);
//                final RequestMappingContext requestMappingContext = new RequestMappingContext(context, handler);
//                resourceGroupRequestMappings.put(resourceGroup, requestMappingContext);
//            }
//            return new ApiListingReferenceScanResult(resourceGroupRequestMappings);
//        }
//    }

    @Component
    public class SwaggerOperationHiddenConfig implements OperationBuilderPlugin {

        @Override
        public void apply(final OperationContext context) {
            final Optional<Api> apiAnnotation = context.findControllerAnnotation(Api.class);
            final Optional<ApiOperation> apiOperationAnnotation = context.findAnnotation(ApiOperation.class);
            // MAKING ALL THE API HIDDEN IF THE CONTROLLER IS MARKED AS HIDDEN
            if (apiAnnotation.isPresent() && apiAnnotation.get().hidden()) {
                context.operationBuilder().hidden(propertyConfig.isHideInternalAPI() ? apiAnnotation.get().hidden() : Boolean.FALSE);
            } else if (apiOperationAnnotation.isPresent()) {
                // THIS WILL HIDE SPECIFIC APIS OF THE CONTROLLER, WHERE HAVING BOTH PRIVATE AND PUBLIC APIS
                context.operationBuilder().hidden(propertyConfig.isHideInternalAPI() ? apiOperationAnnotation.get().hidden() : Boolean.FALSE);
            }
            // APPENDING ASTRISK SIGN TO DIFFERENTIATE PRIVATE APIS
            if (apiAnnotation.get().hidden() || apiOperationAnnotation.get().hidden()) {
                context.operationBuilder().summary(apiOperationAnnotation.get().value() + " *");
            }
        }

        @Override
        public boolean supports(final DocumentationType delimiter) {
            return SwaggerPluginSupport.pluginDoesApply(delimiter);
        }
    }

    public class SwaggerCustomConfig {

        @Value("${POC_API.SWAGGER.DEFAULT_PROTOCOL:https}")
        private String defautProtocol;

        @Value("${POC_API.SWAGGER.BASE_URL}")
        private String baseUrl;

        @Value("${POC_API.SWAGGER.HIDE_INTERNAL:false}")
        private boolean hideInternalAPI;

        public String getDefautProtocol() {
            return defautProtocol;
        }

        public String getBaseUrl() {
            return baseUrl;
        }

        public boolean isHideInternalAPI() {
            return hideInternalAPI;
        }
    }
}
