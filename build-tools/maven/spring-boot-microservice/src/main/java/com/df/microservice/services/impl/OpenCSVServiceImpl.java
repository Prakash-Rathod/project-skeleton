/* Copyright © 2021 Deltafixes Tehcnologies. All rights reserved. */
package com.df.microservice.services.impl;

import com.df.microservice.payloads.CustomerCSVBean;
import com.df.microservice.services.CustomerService;
import com.df.microservice.services.OpenCSVService;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.IOException;

import static com.df.microservice.constants.ApplicationConstants.MEDIA_TYPE_TEXT_CSV;

@Service
@Transactional
public class OpenCSVServiceImpl implements OpenCSVService {

    private final CustomerService customerService;

    @Autowired
    public OpenCSVServiceImpl(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public void importCSV(HttpServletResponse httpServletResponse) {

    }

    @Override
    public void exportCSV(HttpServletResponse httpServletResponse) throws CsvRequiredFieldEmptyException, CsvDataTypeMismatchException, IOException {
        final var CSV_FILE_NAME = "customer.csv";
        var customerData = customerService.getExportCustomerData();
        httpServletResponse.setContentType(MEDIA_TYPE_TEXT_CSV);
        httpServletResponse.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + CSV_FILE_NAME + "\"");
        var beanToCsv = new StatefulBeanToCsvBuilder<CustomerCSVBean>(httpServletResponse.getWriter()).build();
        beanToCsv.write(customerData);
        httpServletResponse.getWriter().flush();
        httpServletResponse.getWriter().close();
    }
}
