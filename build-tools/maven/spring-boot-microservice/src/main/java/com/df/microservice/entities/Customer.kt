/* Copyright © 2021 Deltafixes Tehcnologies. All rights reserved. */
package com.df.microservice.entities

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import org.springframework.format.annotation.DateTimeFormat
import java.time.LocalDate
import javax.persistence.*
import javax.validation.Valid
import javax.validation.constraints.*

/**
 * Customer entity as well as DTO class.
 * For bean validation please read <a href="https://docs.jboss.org/hibernate/validator/4.1/reference/en-US/html/validator-usingvalidator.html#validator-defineconstraints-builtin">more</a>
 */
@ApiModel(description = "Customer DTO")
@Entity
data class Customer(
    @get:JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @get:ApiModelProperty(notes = "Customer Id", example = "1001", hidden = true)
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var cId: Long,

    @ApiModelProperty(notes = "Email address", example = "dummy@gmail.com")
    @Column(name = "emailAddress", unique = true)
    @get:Email(message = "{invalid} 'email'")
    @get:NotBlank(message = "'email' {not-blank}")
    val email: String,

    @ApiModelProperty(notes = "Password", example = "peter@123")
    @Column(length = 200)
    @get:Size(min = 5, max = 200, message = "{min.max.len-under-exceed}")
    @get:NotBlank(message = "'password' {not-blank}")
    var password: String,

    @ApiModelProperty(notes = "Name", example = "peter")
    @Column(length = 30)
    @get:Size(min = 3, max = 30, message = "{min.max.len-under-exceed}")
    @get:NotBlank(message = "'name' {not-blank}")
    val name: String,

    @ApiModelProperty(notes = "Phone Number", example = "1234567890")
    @Column(length = 20)
    @get:Pattern(regexp = "(^$|[0-9]{10})", message = "{invalid} 'phoneNumber'")
    @get:NotBlank(message = "'phoneNumber' {not-blank}")
    val phoneNumber: String,


    @ApiModelProperty(notes = "Date Of Birth (yyyy-MM-dd)", example = "1990-04-25")
    @Column(length = 10)
    @get:Past(message = "{dob.invalid}")
    @get:DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    val dateOfBirth: LocalDate,

    @get:JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @OneToMany(cascade = [CascadeType.ALL], orphanRemoval = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_c_id")
    @get:Valid
    var addresses: List<Address>
)

/**
 * Customer address entity as well as DTO class
 */
@ApiModel(description = "Address DTO")
@Entity
@Table(
    uniqueConstraints = [
        UniqueConstraint(columnNames = ["customer_c_id", "address_type"])
    ]
)
data class Address(
    @get:JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @ApiModelProperty(notes = "Address Id", example = "1001")
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    val aId: Long,

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "customer_c_id")
    val customer: Customer,

    @ApiModelProperty(notes = "Address Type (Mailing, Physical)", example = "Mailing")
    @Column(name = "address_type", length = 30)
    @get:Size(max = 30, message = "{max.len-exceed}")
    @get:NotBlank(message = "'addressType' {not-blank}")
    val addressType: String,

    @ApiModelProperty(notes = "Address 1", example = "1201, Gold Tower")
    @Column(length = 100)
    @get:Size(max = 100, message = "{max.len-exceed}")
    @get:NotBlank(message = "'address1' {not-blank}")
    val address1: String,

    @ApiModelProperty(notes = "Address 2", example = "near reliance mall, science city road")
    @Column(length = 100)
    @get:Size(max = 100, message = "'address2' {max.len-exceed}")
    val address2: String,

    @ApiModelProperty(notes = "City", example = "Ahmedabad")
    @Column(length = 50)
    @get:Size(max = 50, message = "{max.len-exceed}")
    @get:NotBlank(message = "'city' {not-blank}")
    val city: String,

    @ApiModelProperty(notes = "State", example = "Gujarat")
    @Column(length = 50)
    @get:Size(max = 50, message = "{max.len-exceed}")
    @get:NotBlank(message = "'state' {not-blank}")
    val state: String,

    @ApiModelProperty(notes = "Nationality", example = "India")
    @Column(length = 50)
    @get:Size(max = 50, message = "{max.len-exceed}")
    @get:NotBlank(message = "'nationality' {not-blank}")
    val nationality: String,

    @ApiModelProperty(notes = "PinCode", example = "380060")
    @Column(length = 6)
    @get:Size(min = 6, max = 6, message = "{min.max.len-under-exceed}")
    @get:NotBlank(message = "'pincode' {not-blank}")
    val pincode: String,
)