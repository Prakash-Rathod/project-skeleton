package com.df.microservice.services;

import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface OpenCSVService {
    void exportCSV(HttpServletResponse httpServletResponse) throws CsvRequiredFieldEmptyException, CsvDataTypeMismatchException, IOException;

    void importCSV(HttpServletResponse httpServletResponse);
}
