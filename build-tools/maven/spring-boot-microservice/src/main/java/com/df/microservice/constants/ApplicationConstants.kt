package com.df.microservice.constants

/**
 * Whole application constant fields.
 */
object ApplicationConstants {
    const val MEDIA_TYPE_TEXT_CSV = "text/csv"
}