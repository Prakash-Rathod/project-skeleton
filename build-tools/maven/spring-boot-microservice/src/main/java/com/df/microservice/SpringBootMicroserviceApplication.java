/* Copyright © 2021 Deltafixes Tehcnologies. All rights reserved. */
package com.df.microservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Entry point of the application startup.
 */
@SpringBootApplication
public class SpringBootMicroserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMicroserviceApplication.class, args);
    }
}
