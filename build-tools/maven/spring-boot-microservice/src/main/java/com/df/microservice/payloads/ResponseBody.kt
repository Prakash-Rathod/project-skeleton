package com.df.microservice.payloads

import com.fasterxml.jackson.annotation.JsonInclude
import io.swagger.annotations.ApiModelProperty

@JsonInclude(JsonInclude.Include.NON_NULL)
data class SuccessResponseBody(
    @ApiModelProperty(notes = "HTTP statues code.", example = "200")
    val statusCode: Int,
    @ApiModelProperty(notes = "HTTP statues text.", example = "OK")
    val statusText: String,
    @ApiModelProperty(notes = "Success message")
    val message: String,
    val success: DataResponseBody?
)

@JsonInclude(JsonInclude.Include.NON_NULL)
data class FaultResponseBody(
    @ApiModelProperty(notes = "HTTP statues code.")
    val statusCode: Int,
    @ApiModelProperty(notes = "HTTP statues text.")
    val statusText: String,
    val error: FaultResponse?
)

data class DataResponseBody(
    val data: Any
)

data class FaultResponse(
    @ApiModelProperty(notes = "Exception message.")
    val message: String,
    @ApiModelProperty(notes = "HttpRequest path.")
    val path: String
)
