package com.df.microservice.controllers;

import com.df.microservice.annotations.ExportApiResponseDoc;
import com.df.microservice.constants.ApiConstant;
import com.df.microservice.constants.SwaggerConstant;
import com.df.microservice.services.OpenCSVService;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.df.microservice.constants.ApplicationConstants.MEDIA_TYPE_TEXT_CSV;

/**
 * Export & Import controller are mainly expose  REST end points for export/import data with different file format.
 */

@Api(tags = {SwaggerConstant.ApiTag.EXPORT_IMPORT}, value = "Export and Import")
@RestController
@Validated
@RequestMapping(value = ApiConstant.API_EXPORT_IMPORT)
public class ExportImportController {

    private final OpenCSVService openCSVService;

    @Autowired
    public ExportImportController(OpenCSVService openCSVService) {
        this.openCSVService = openCSVService;
    }

    @ExportApiResponseDoc
    @ApiOperation(value = "Export as CSV", notes = "Export as CSV", response = String.class)
    @RequestMapping(value = "/opencsv/export", method = RequestMethod.GET, produces = MEDIA_TYPE_TEXT_CSV)
    public void exportUsingOpenCsv(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws CsvRequiredFieldEmptyException, CsvDataTypeMismatchException, IOException {
        openCSVService.exportCSV(httpServletResponse);
    }
}
