package com.df.microservice.controllers;

import com.df.microservice.annotations.ApiResponseDoc;
import com.df.microservice.component.Messages;
import com.df.microservice.configurations.jwt.JwtUtil;
import com.df.microservice.constants.ApiConstant;
import com.df.microservice.constants.SwaggerConstant;
import com.df.microservice.payloads.DataResponseBody;
import com.df.microservice.payloads.JwtRequest;
import com.df.microservice.payloads.JwtResponse;
import com.df.microservice.payloads.SuccessResponseBody;
import com.df.microservice.services.CustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.OK;

/**
 * Authentication controller.
 */

@Api(tags = {SwaggerConstant.ApiTag.AUTHENTICATION}, value = "Authentication")
@RestController
@Validated
@RequestMapping(value = ApiConstant.API_AUTHENTICATION)
public class JwtAuthenticationController {

    private final AuthenticationManager authenticationManager;
    private final JwtUtil jwtTokenUtil;
    private final UserDetailsService userDetailsService;
    private final Messages message;
    private final CustomerService customerService;

    @Autowired
    public JwtAuthenticationController(AuthenticationManager authenticationManager, JwtUtil jwtTokenUtil,
                                       UserDetailsService userDetailsService, Messages message, CustomerService customerService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userDetailsService = userDetailsService;
        this.message = message;
        this.customerService = customerService;
    }

    @ApiResponseDoc
    @ApiOperation(value = "Get Auth token", notes = "Get Auth token", response = SuccessResponseBody.class)
    @RequestMapping(value = "/authenticate", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createAuthenticationToken(@Valid @RequestBody JwtRequest jwtRequest)
            throws BadCredentialsException {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(jwtRequest.getUserName(), jwtRequest.getPassword()));
        } catch (BadCredentialsException e) {
            throw new BadCredentialsException(message.get("request.unauthorized"));
        }
        final var userDetails = userDetailsService.loadUserByUsername(jwtRequest.getUserName());
        final var token = jwtTokenUtil.generateToken(userDetails);
        final var customer = customerService.findByEmail(userDetails.getUsername());
        return new ResponseEntity<>(new SuccessResponseBody(OK.value(), OK.name(), message.get("data.found"), new DataResponseBody(new JwtResponse(token, customer))), OK);
    }
}
