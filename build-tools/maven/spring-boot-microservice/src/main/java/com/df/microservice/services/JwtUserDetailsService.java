/* Copyright © 2021 Deltafixes Tehcnologies. All rights reserved. */
package com.df.microservice.services;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * JWT user detail service and their implementation are
 *
 * @see com.df.microservice.services.impl.JwtUserDetailsServiceImpl
 */
public interface JwtUserDetailsService {
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
}
