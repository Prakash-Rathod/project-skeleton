/* Copyright © 2021 Deltafixes Tehcnologies. All rights reserved. */
package com.df.microservice.events;

import com.df.microservice.entities.Customer;
import com.df.microservice.services.CustomerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Application events, initiate based on event listener
 */
@Configuration
public class ApplicationEvents {

    private final CustomerService customerService;
    private final ObjectMapper objectMapper;
    private final PasswordEncoder passwordEncoder;
    private final String customerDefaultEntryJsonString;
    private final Logger logger = LoggerFactory.getLogger(ApplicationEvents.class);

    @Autowired
    public ApplicationEvents(CustomerService customerService, ObjectMapper objectMapper,
                             PasswordEncoder passwordEncoder, @Value("${customer.default.entry}") String customerDefaultEntryJsonString) {
        this.customerService = customerService;
        this.objectMapper = objectMapper;
        this.passwordEncoder = passwordEncoder;
        this.customerDefaultEntryJsonString = customerDefaultEntryJsonString;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void onReadyApplicationEvent() throws JsonProcessingException {
        // Creating application default user.
        try {
            objectMapper.registerModule(new JavaTimeModule());
            Customer customer = objectMapper.readValue(customerDefaultEntryJsonString, Customer.class);
            customer.setPassword(passwordEncoder.encode(customer.getPassword()));
            customerService.save(customer);
            logger.info(String.format("Default user created with userName: %s and password: %s", customer.getEmail(), customer.getPassword()));
        } catch (Exception e) {
            logger.info("Default user already exist.");
        }
    }
}
