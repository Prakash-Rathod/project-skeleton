/* Copyright © 2021 Deltafixes Tehcnologies. All rights reserved. */
package com.df.microservice.exceptions

/**
 * Throwing custom exception `RecordNotFoundException` with custom message, It will be thrown when record not found in to the table.
 * And also these exceptions will handle in
 * @see GlobalExceptionHandler
 */

class RecordNotFoundException(message: String?) : RuntimeException(message)
class SomethingWrongException(message: String?) : RuntimeException(message)