package com.df.microservice.exceptions;

import com.df.microservice.payloads.FaultResponse;
import com.df.microservice.payloads.FaultResponseBody;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.sql.SQLException;

import static org.springframework.http.HttpStatus.*;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return new ResponseEntity<>(new FaultResponseBody(status.value(), status.name(),
                new FaultResponse(ex.getLocalizedMessage(), request.getDescription(true))), status);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return new ResponseEntity<>(new FaultResponseBody(status.value(), status.name(),
                new FaultResponse(ex.getLocalizedMessage(), request.getDescription(true))), status);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String error = ex.getBindingResult().getAllErrors().stream().map(ObjectError::getDefaultMessage).findFirst().orElse("");
        return new ResponseEntity<>(new FaultResponseBody(status.value(), status.name(),
                new FaultResponse(error, request.getDescription(true))), status);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public final ResponseEntity<?> constraintViolationException(Exception ex, WebRequest request) {
        return new ResponseEntity<>(new FaultResponseBody(BAD_REQUEST.value(), BAD_REQUEST.name(),
                new FaultResponse(ex.getLocalizedMessage(), request.getDescription(true))), BAD_REQUEST);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public final ResponseEntity<?> dataIntegrityViolationException(SQLException ex, WebRequest request) {
        return new ResponseEntity<>(new FaultResponseBody(BAD_REQUEST.value(), BAD_REQUEST.name(),
                new FaultResponse(String.format("%d : %s", ex.getErrorCode(), ex.getLocalizedMessage()), request.getDescription(true))), BAD_REQUEST);
    }

    @ExceptionHandler(RecordNotFoundException.class)
    public final ResponseEntity<?> runtimeException(RuntimeException ex, WebRequest request) {
        return new ResponseEntity<>(new FaultResponseBody(NOT_FOUND.value(), NOT_FOUND.name(),
                new FaultResponse(ex.getLocalizedMessage(), request.getDescription(true))), NOT_FOUND);
    }

    @ExceptionHandler(SomethingWrongException.class)
    public final ResponseEntity<?> internalServerError(RuntimeException ex, WebRequest request) {
        return new ResponseEntity<>(new FaultResponseBody(INTERNAL_SERVER_ERROR.value(), INTERNAL_SERVER_ERROR.name(),
                new FaultResponse(ex.getLocalizedMessage(), request.getDescription(true))), INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(BadCredentialsException.class)
    public final ResponseEntity<Object> handleBadCredentialsException(Exception ex, WebRequest request) {
        return new ResponseEntity<>(new FaultResponseBody(UNAUTHORIZED.value(), UNAUTHORIZED.name(),
                new FaultResponse(ex.getLocalizedMessage(), request.getDescription(true))), UNAUTHORIZED);
    }
}
