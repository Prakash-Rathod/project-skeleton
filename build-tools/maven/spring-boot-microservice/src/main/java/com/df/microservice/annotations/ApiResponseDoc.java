/* Copyright © 2021 Deltafixes Tehcnologies. All rights reserved. */
package com.df.microservice.annotations;

import com.df.microservice.payloads.FaultResponseBody;
import com.df.microservice.payloads.SuccessResponseBody;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;

import java.lang.annotation.*;

/**
 * This is the custom annotation, will help us to document all API's endpoint responses.
 * When new error message handled throughout the project, must be configured over here also.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success", response = SuccessResponseBody.class),
        @ApiResponse(code = 400, message = "Bad request", response = FaultResponseBody.class,
                examples = @Example(value = @ExampleProperty(value = "{'statusCode':400,'statusText':'BAD_REQUEST','error':{'message':'JSON parse error: Unrecognized token ...'," +
                        "'path':'uri=/xyz/action;client=0:0:0:0:0:0:0:1'}}", mediaType = "application/json"))),
        @ApiResponse(code = 401, message = "Unauthorized", response = FaultResponseBody.class,
                examples = @Example(value = @ExampleProperty(value = "{'statusCode':401,'statusText':'UNAUTHORIZED','error':{'message':'The request could not be authenticated'," +
                        "'path':'uri=/xyz/action;client=0:0:0:0:0:0:0:1'}}", mediaType = "application/json"))),
        @ApiResponse(code = 404, message = "Not found", response = FaultResponseBody.class,
                examples = @Example(value = @ExampleProperty(value = "{'statusCode':404,'statusText':'NOT_FOUND','error':{'message':'Didn't find customer with Id 1000001'," +
                        "'path':'uri=/xyz/action/1000001;client=0:0:0:0:0:0:0:1'}}", mediaType = "application/json"))),
        @ApiResponse(code = 415, message = "Unsupported Media Type", response = FaultResponseBody.class,
                examples = @Example(value = @ExampleProperty(value = "{'statusCode':415,'statusText':'UNSUPPORTED_MEDIA_TYPE','error':{'message':'Unsupported Media Type'," +
                        "'path':'uri=/xyz/action;client=0:0:0:0:0:0:0:1'}}", mediaType = "application/json"))),
        @ApiResponse(code = 500, message = "Internal Server Error", response = FaultResponseBody.class,
                examples = @Example(value = @ExampleProperty(value = "{'statusCode':500,'statusText':'INTERNAL_SERVER_ERROR','error':{'message':'Something went wrong please try again'," +
                        "'path':'uri=/xyz/action;client=0:0:0:0:0:0:0:1'}}", mediaType = "application/json")))
})
public @interface ApiResponseDoc {
}

