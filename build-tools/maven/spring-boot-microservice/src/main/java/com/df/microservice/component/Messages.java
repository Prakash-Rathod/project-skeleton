/* Copyright © 2021 Deltafixes Tehcnologies. All rights reserved. */
package com.df.microservice.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Locale;

/**
 * Configure i18n properties, and read properties under `/i18n/` based on language code.
 */
@Component
public class Messages {
    @Autowired
    private MessageSource messageSource;

    private MessageSourceAccessor accessor;

    @PostConstruct
    private void init() {
        accessor = new MessageSourceAccessor(messageSource, Locale.US);
    }

    public String get(String key) {
        return accessor.getMessage(key);
    }

    public String get(String key, Object... args) {
        return accessor.getMessage(key, args);
    }
}
