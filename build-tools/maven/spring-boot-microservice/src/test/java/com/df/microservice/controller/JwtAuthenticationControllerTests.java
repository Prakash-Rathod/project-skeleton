package com.df.microservice.controller;

import com.df.microservice.controllers.CustomerController;
import com.df.microservice.entities.Customer;
import com.df.microservice.services.CustomerService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


//@AutoConfigureMockMvc
//@ContextConfiguration(classes = {CustomerService.class})
//@WebMvcTest(value = CustomerController.class)
@ExtendWith(SpringExtension.class)
@WebMvcTest(CustomerController.class)
public class JwtAuthenticationControllerTests {

    @MockBean
    private CustomerService customerService;

    @Autowired
    private MockMvc mockMvc;

    //@Autowired
    //private ObjectMapper objectMapper;

    @Test
    public void testAuthentication() throws Exception {
        //        SuccessResponseBody response = new SuccessResponseBody(200, "Success", "test",
//                new DataResponseBody(new JwtResponse("test", new Customer(101, "test@gmail.com", "test", "", "", LocalDate.now(), null))));

        StringBuilder builder = new StringBuilder("TERe");
        builder.reverse();
        Customer customer = new Customer(101, "test@gmail.com", "test", "peter", "8080908989", LocalDate.now(), null);
        List<Customer> customers = Arrays.asList(customer);
        Mockito.when(customerService.getAll()).thenReturn(customers);

        mockMvc.perform(MockMvcRequestBuilders.get("/microservice/customer/all"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].name", Matchers.is("peter")));

//        JwtRequest jwtRequest = new JwtRequest("peter@gmail.com", "peter@123");
//        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/microservice/customer/all")
//                        .with(user("peter@gmail.com"))
//                        .with(csrf())
//                        .content("{ \"password\": \"peter@123\", \"userName\": \"peter@gmail.com\"}")
//                        .header(JwtUtil.AUTHORIZATION, "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwZXRlckBnbWFpbC5jb20iLCJleHAiOjE2MzU4NTYxNjcsImlhdCI6MTYzNTgzODE2N30.ve_ZaLqH0JSs8ClkCRjjD0v7hqyon6_YmmJHKxh9FAlog5cJCH2K8zszQyp-akxmIxzpEd--o159_1kzrxybyQ")
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .accept(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andReturn();

        //String resultDOW = result.getResponse().getContentAsString();
        //assertNotNull(resultDOW);
        //assertEquals(dow, resultDOW);
    }
}
