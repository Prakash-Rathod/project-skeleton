package com.df.microservice;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * Write a Test class annotated with @SpringBootTest and check any bean if it has been successfully injected into an auto-wired attribute or not.
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
class SpringBootMicroserviceApplicationTests {

    //@Autowired
    //JwtAuthenticationController jwtAuthenticationController;

    //@Test
    void contextLoads() {
        //Assertions.assertThat(jwtAuthenticationController).isNot(null);
    }

}
