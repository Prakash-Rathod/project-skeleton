# SPRING BOOT MICROSERVICES #

Microservices with different features.

### Software Versions

```
    1. Red Hat OpenJDK 11
    2. MySQL 5.7
    3. Maven 3.6.3
    4. IntelliJ IDEA 2021.2 (Edu) with google-java-format plugin configued
```

### Technologies

* [Spring Boot Framework](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/)
* OpenJDK 11 + [Kotlin 1.5.31](https://kotlinlang.org/)

### Features

* Bean validations.
* Custom & Global exception handling.
* Custom annotation.
* CRUD operation between relational entity.
* Microservices with java & swagger documentation.
* Request & Response standard format for whole application.
* Swagger API configurations.
* i18n messages.
* Named & Named native query(Load from xml)
* Application events such as StartingEvent, EnvironmentPreparedEvent, StartedEvent, onReadyEvent etc
* JWT Authentication

## Folder Structure

```
spring-boot-microservices
├── src
│ ├── main
│ │ ├── java
│ │ │ └── com.df.microservice
│ │ │   ├── annotations
│ │ │   │ ├── ApiResponseDoc.java
│ │ │   │ └── ExportApiResponseDoc.java
│ │ │   ├── component
│ │ │   │ └── Messages.java
│ │ │   ├── configurations
│ │ │   │ ├── jwt
│ │ │   │ │ ├── JwtAuthenticationEntryPoint.java
│ │ │   │ │ ├── JwtRequestFilter.java
│ │ │   │ │ ├── JwtUtil.java
│ │ │   │ │ └── WebSecurityConfiguration.java
│ │ │   │ ├── ApplicationConfiguration.java
│ │ │   │ └── SwaggerDocumentation.java
│ │ │   ├── controllers
│ │ │   │ ├── CustomerController.java
│ │ │   │ ├── ExportImportController.java
│ │ │   │ └── JwtAuthenticationController.java
│ │ │   ├── constants
│ │ │   │ └── ApplicationConstants.java
│ │ │   ├── entities
│ │ │   │ └── ApplicationEvents.java
│ │ │   ├── events
│ │ │   │ └── Customer.kt
│ │ │   ├── exceptions
│ │ │   │ ├── CustomException.kt
│ │ │   │ └── GlobalExceptionHandler.java
│ │ │   ├── payloads
│ │ │   │ ├── CustomerCSVBean.kt
│ │ │   │ ├── JsonWebToken.kt
│ │ │   │ └── ResponseBody.kt 
│ │ │   ├── repositories
│ │ │   │ └── CustomerRepository.java
│ │ │   ├── services
│ │ │   │   ├── CustomerService.java
│ │ │   │   ├── JwtUserDetailsService.java
│ │ │   │   ├── OpenCSVService.java 
│ │ │   │   └── impl
│ │ │   │     ├── CustomerServiceImpl.java
│ │ │   │     ├── JwtUserDetailsServiceImpl.java
│ │ │   │     └── OpenCSVServiceImpl.java
│ │ │   └── SpringBootMicroserviceApplication.java
│ │ └── resources
│ │   ├── i18n
│ │   │ └── messages_en.properties
│ │   ├── META-INF
│ │   │ └── orm.xml
│ │   ├── application.properties
│ │   └── banner.txt
│ └── test
│   └── java
│     └── com.df.microservice
│        └── SpringBootMicroserviceApplicationTests.java
├── HELP.md
├── mvnw
├── mvnw.cmd
├── pom.xml
└── README.md
```

###### 1. com.df.microservice

The root directory of the microservice application.
> SpringBootMicroserviceApplication.java
>> Entry point of the application startup.

###### 2. com.df.microservice.annotations

This package contains all custom annotations, which are developed by our hand.

> ApiResponseDoc.java
>> This annotation should be used in REST method level by @ApiResponseDoc, so that it will be document response code which are configured under ``ApiResponseDoc.java``. Ensure that for new error message handled throughout the project, must be configured under ``ApiResponseDoc.java``

###### 3. com.df.microservice.component

Contain injectable services/custom bean configuration.

> Messages.java
>> Configure multi-language properties, and read properties under `/i18n/` based on language code.

###### 4. com.df.microservice.configurations

Application level & other configuration should be under this package only.

> ApplicationConfiguration.java
>> Application level configurations are defined in this class, such as `Multilingual support configuration`, `Bean validation configuration` etc.

> SwaggerDocumentation.java
>> Global API document configuration are defined in this class, such as `Docket`, `ApiInfo`, `UiConfiguration` etc.

###### 5. com.df.microservice.controllers

REST endpoint must be defined in this package only.
> CustomerController.java
>> Customer basic `CRUD` operations has been created. such as `ADD` new customer, `INSERT, UPDATE, DELETE` customer address, `DELETE,FIND, FIND ALL` customer.