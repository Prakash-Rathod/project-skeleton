# GRADLE PROJECT SKELETON #

Gradle Build Tool is an open source build automation system designed for multi-project builds. It supports Maven and Ivy repositories for retrieving dependencies so that you can reuse artifacts of existing build systems.
Essentially, Gradle was built to combine the best parts of established tools like Ant and Maven, but with a Groovy-based Domain Specific Language (DSL) instead of XML for declaring project configuration. DSL tends to be easier to read, more succinct, and also more powerful.
