# PROJECT SKELETON #

This README would normally document high level project skeleton description. The main aim is to provide basis for
starting project with a good directory structure, documentation, configuration and codding standard, so that this will
help us to quickly develop project from scratch with minor changes. This is not only project skeleton we can get
features wise coed reference, like file upload & download, export & import etc.

## Folder Structure

```
project-skeleton
├── README.md
├── build-tools
│ ├── gradle
│ ├── maven
│ │   ├── axon-food-ordering
│ │   ├── spring-boot-microservice
│ │   └── 
└── 
```

###### Gradle

###### Maven

	1. axon-food-ordering
        This samll food ordering poc are developed in Axon framwork. Read more `/axon-food-ordering/README.md`
    2. spring-boot-microservice
        This skeleton for microservices are developed in Spring Boot framwork. Read more `/spring-boot-microservice/README.md`